<?php 
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route; 


class FrontController extends AbstractController
{
   
 /**
     * @Route( "/", name="index")
     * @Route( "/boule")
     */
    public function index(){
        return $this->render("index.html.twig");
    }
     /**
     * @Route( "/boule/{couleur}", name="boule")
     */
    public function couleur(string $couleur){
        if( $couleur=="rouge" || $couleur=="bleue" || $couleur=="multicolore") return $this->render("boule.html.twig", ["couleur"=>$couleur]);
        return $this->render("404.html.twig", ["couleur"=>$couleur]);
    }

    

   

}