# Exercice Symfony #
## Noel et Cie ##

Pour récupérer cet exercice :

* copiez la ligne de clone (bouton cloner en haut à droite)
* dans votre invite de commande, déplacez vous dans le repertoire dans lequel vous voulez créer le projet et copier la ligne
* Cela vous a créé un dossier "bouleDeNoel", déplacez vous dedans

```cd bouleDeNoel```

* lancez l'installation composer afin de récupérer toutes vos librairies

```composer install ``
